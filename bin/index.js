#!/usr/bin/env node
const colors = require('colors');
const { frontend, backend } = require('../lib/framework');

var arguments = process.argv.splice(2);

var framework = null;

if(arguments.length > 0) {
  switch(arguments[0]) {
    case 'frontend':
      framework = arguments[1] || 'HTML';
      console.log( frontend(framework) )
      break;
    case 'backend':
      framework = arguments[1] || 'koa-typescript';
      console.log( backend(framework) )
      break;
  }
} else {
  console.log(colors.rainbow('Es necesario escribir parametros'));
}
