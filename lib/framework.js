const _ = require("lodash");
const colors = require('colors');
const { spawn, exec } = require('child_process');

module.exports = {
  frontend: (arg) => {
    if(!arg) return 'Deberá de poner un argumento';

    switch(arg){
      case 'react':
        return 'esto es react'
      case 'angular':
        processCloneGit('https://jlunasolera@bitbucket.org/soleramobile/solera_frontend_base_angular.git');
        return colors.red('Framework Angular');
      default:
        return 'tiene que especificar el framework'
    }
  },
  backend: (arg) => {
    if(!arg) return 'Deberá de poner un argumento';

    switch(arg) {
      case 'koa-typescript':
        processCloneGit('https://soleramobile@bitbucket.org/soleramobile/solera_backend_base_koa_typescript.git');
        return colors.green('Framework Koa con Typescript');
      default:
        return 'tiene que especificar el framework'
    }
  }
}

function processCloneGit(url) {
  const gitClone = spawn('git', ['clone', url, '.'], {'cwd': './'});

  gitClone.stdout.on('data', (data) => {
    console.log(`stdout: ${data.toString()}`);
  });

  gitClone.stderr.on('data', (data) => {
    if( !(data.toString() == "fatal: destination path '.' already exists and is not an empty directory.") ) {
      console.error(`${data.toString()}`);
    }
  })

  gitClone.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    removeGit();
    installDependencies();
  });

}

function removeGit() {
  exec('rm -r .git', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    if(stdout) console.log(`stdout: ${stdout}`);
    if(stderr) console.error(`stderr: ${stderr}`);
  });
}
function installDependencies() {
  exec('npm i', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    if(stderr) console.error(`stderr: ${stderr}`);
  });
}
